import "./App.css";
import { AddKontak } from "./components";
import ListKontak from "./components/listKontak";

function App() {
  return (
    <div className="App">
      <h2 style={{ padding: "30px" }}>Aplikasi Kontak App</h2>
      <hr />
      <AddKontak />
      <hr />
      <ListKontak />
    </div>
  );
}

export default App;
