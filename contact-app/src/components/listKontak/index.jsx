import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteKontak, getListKontak, detailKontak } from "../../actions/kontakAction";

const ListKontak = () => {
  const { getListKontakResult, deleteKontakResult } = useSelector((state) => state.KontakReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListKontak());
  }, [dispatch]);

  useEffect(() => {
    if (deleteKontakResult) {
      dispatch(getListKontak());
    }
  }, [deleteKontakResult, dispatch]);

  return (
    <div>
      <h4>List Kontak</h4>
      {getListKontakResult
        ? getListKontakResult.map((kontak) => {
            return (
              <p key={kontak.id}>
                {kontak.nama} -{kontak.nohp}
                <button onClick={() => dispatch(deleteKontak(kontak.id))}>Hapus</button>
                <button style={{ marginLeft: "10px" }} onClick={() => dispatch(detailKontak(kontak))}>
                  Edit
                </button>
              </p>
            );
          })
        : "Data not found brader"}
    </div>
  );
};

export default ListKontak;
