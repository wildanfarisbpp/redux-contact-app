import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addKontak, detailKontak, getListKontak, updateKontak } from "../../actions/kontakAction";

const AddKontak = () => {
  const [nama, setNama] = useState("");
  const [nohp, setNohp] = useState("");
  const [id, setId] = useState("");

  const { addKontakResult, detailKontakResult, updateKontakResult } = useSelector((state) => state.KontakReducer);
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    if (id) {
      dispatch(updateKontak({ id, nama, nohp }));
    } else {
      dispatch(addKontak({ nama, nohp }));
    }
  };

  useEffect(() => {
    if (addKontakResult) {
      dispatch(getListKontak());
      setNama("");
      setNohp("");
    }
  }, [addKontakResult, dispatch]);

  useEffect(() => {
    if (detailKontakResult) {
      setNama(detailKontakResult.nama);
      setNohp(detailKontakResult.nohp);
      setId(detailKontakResult.id);
    }
  }, [detailKontakResult, dispatch]);

  useEffect(() => {
    if (updateKontakResult) {
      dispatch(getListKontak());
      setNama("");
      setNohp("");
      setId("");
    }
  }, [updateKontakResult, dispatch]);

  return (
    <section>
      <h4>{id ? "Update Kontak" : "Add Kontak"}</h4>
      <form onSubmit={(event) => handleSubmit(event)}>
        <input type="text" name="nama" placeholder="nama..." value={nama} onChange={(event) => setNama(event.target.value)} />
        <input type="text" name="nohp" placeholder="nohp..." value={nohp} onChange={(event) => setNohp(event.target.value)} />
        <button type="submit">Submit</button>
      </form>
    </section>
  );
};

export default AddKontak;
